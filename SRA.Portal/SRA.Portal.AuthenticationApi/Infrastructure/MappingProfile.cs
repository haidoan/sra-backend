﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using SRA.Portal.AuthenticationApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRA.Portal.AuthenticationApi.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<UserModel, ApplicationUser>();
            CreateMap<ApplicationUser, UserModel>();

            CreateMap<UserManager<UserModel>, UserManager<ApplicationUser>>();
            CreateMap<UserManager<ApplicationUser>, UserManager<UserModel>>();
        }
    }
}
