﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRA.Portal.AuthenticationApi.Models.ResponseModels
{
    public class LoginResponsePayloadModel
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public long expires_in { get; set; }
    }
}
