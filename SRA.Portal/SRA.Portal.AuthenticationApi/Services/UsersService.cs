﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using SRA.Portal.AuthenticationApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRA.Portal.AuthenticationApi.Services
{
    public class UsersService : IUsersService
    {
        private readonly IMapper _mapper;
        private readonly IUserValidator<ApplicationUser> _userValidator;
        private readonly IPasswordValidator<ApplicationUser> _passwordValidator;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UsersService(IMapper mapper, UserManager<ApplicationUser> userManager, IUserValidator<ApplicationUser> userValidator, IPasswordValidator<ApplicationUser> passwordValidator, IPasswordHasher<ApplicationUser> passwordHasher, SignInManager<ApplicationUser> signInManager)
        {

            _userManager = userManager;
            _userValidator = userValidator;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public IQueryable<UserModel> Get()
        {

            var returnedList = new List<UserModel>();
            _userManager.Users.ToList().ForEach(u =>
            {
                returnedList.Add(_mapper.Map<ApplicationUser, UserModel>(u));
            });

            return returnedList.AsQueryable();
        }

        public UserModel GetByEmail(string email)
        {
            return _mapper.Map<ApplicationUser, UserModel>(_userManager.Users.FirstOrDefault(u => u.Email == email));
        }

        public Task<IdentityResult> Create(UserModel user, string password)
        {
            return _userManager.CreateAsync(_mapper.Map<ApplicationUser, UserModel>(user), password);
        }

        public Task<IdentityResult> Create(ApplicationUser user, string password)
        {
            return _userManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> Delete(UserModel user)
        {
            return await _userManager.DeleteAsync(_mapper.Map<ApplicationUser, UserModel>(user));
        }

        public async Task<IdentityResult> Update(UserModel user)
        {
            return await _userManager.UpdateAsync(_mapper.Map<ApplicationUser, UserModel>(user));
        }

        public async Task<IdentityResult> ValidatePassword(UserModel user, string password)
        {
            var appUser = _mapper.Map<ApplicationUser, UserModel>(user);
            return await _passwordValidator.ValidateAsync(_userManager, appUser, password);
        }

        public async Task<IdentityResult> ValidateUser(UserModel user)
        {
            var appUser = _mapper.Map<ApplicationUser, UserModel>(user);
            return await _userValidator.ValidateAsync(_userManager, appUser);
        }

        public string HashPassword(UserModel user, string password)
        {
            var appUser = _mapper.Map<ApplicationUser, UserModel>(user);
            return _passwordHasher.HashPassword(appUser, password);
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<SignInResult> PasswordSignInAsync(UserModel user, string password, bool lockoutOnFailure, bool isPersistent)
        {
            var appUser = _mapper.Map<ApplicationUser, UserModel>(user);
            return await _signInManager.PasswordSignInAsync(appUser, password, isPersistent, lockoutOnFailure);
        }

    }
}
