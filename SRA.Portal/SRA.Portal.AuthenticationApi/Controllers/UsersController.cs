using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SRA.Portal.AuthenticationApi.Models;
using System.Net.Http;
using SRA.Portal.AuthenticationApi.ErrorHandler;
using SRA.Portal.AuthenticationApi.Services;
using SRA.Portal.AuthenticationApi.Models.AccountViewModels;
using Microsoft.AspNetCore.Cors;
using System.Net;
using SRA.Portal.AuthenticationApi.Models.ResponseModels;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace SRA.Portal.AuthenticationApi.Controllers
{
    [Produces("application/json")]
    [EnableCors("CorsPolicy")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IConfigurationRoot _configuration;

        private readonly IErrorHandler _errorHandler;

        private readonly HttpClient client = new HttpClient();


        public UsersController(IUsersService usersService, IConfigurationRoot configuration, IErrorHandler errorHandler)
        {
            _usersService = usersService;
            _configuration = configuration;
            _errorHandler = errorHandler;
        }

        [HttpGet]
        public List<UserModel> Get()
        {
            return null;
        }

        [HttpPost("/api/[controller]/login")]
        public async Task<ReponseModel> Login([FromBody]LoginViewModel loginModel)
        {

            if (!ModelState.IsValid)
            {
                throw new HttpRequestException(string.Format(_errorHandler.GetMessage(ErrorMessagesEnum.ModelValidation), "", ModelState.Values.First().Errors.First().ErrorMessage));
            }

            var user = _usersService.GetByEmail(loginModel.Email);

            if (user == null)
                throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthUserDoesNotExists));

            await _usersService.SignOutAsync();
            var result = await _usersService.PasswordSignInAsync(
                user, loginModel.Password, false, false);

            if (result.Succeeded)
            {
                return new SuccessReponseModel
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Login successful.",
                    Payload = new LoginResponsePayloadModel
                    {
                        access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lSWQiOjcyLCJ1bmlxdWVOYW1lIjoiaGFpLmRvYW4iLCJleHBpcnkiOiJcL0RhdGUoMTQ5NjkwNDkyMjczMSlcLyIsIkZpcnN0TmFtZSI6IkVtZXJ5IiwiTGFzdE5hbWUiOiJOYXZhcnJvIiwiUHJlZmVyZW5jZXMiOnsiVGhlbWVDb2xvciI6IiIsIkZvbnRTaXplIjoiIn19.MEIpU0Lnbeven3c46KRvLHxCNNAWpv-zfwH2ZGV6JjA",
                        refresh_token = "f93d9bb714cd4410bf1a6e64f956b2dd",
                        expires_in = 59999
                    }
                };
            }

            throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthWrongCredentials));
        }

        [HttpGet("{email}")]
        public UserModel Get(string email)
        {
            return _usersService.GetByEmail(email);
        }


        [HttpPost("/api/[controller]/create")]
        public async Task<ReponseModel> Create([FromBody]RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpRequestException(string.Format(
                    _errorHandler.GetMessage(ErrorMessagesEnum.ModelValidation),
                    ModelState.Values.First().Errors.First().ErrorMessage));
            }
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                PhoneNumberConfirmed = model.PhoneNumberConfirmed
            };

            var result = await _usersService.Create(user, model.Password);

            if (result.Succeeded)
            {
                var userModel = _usersService.GetByEmail(model.Email);

                //Integration with service bus
                client.BaseAddress = new Uri(_configuration.GetSection("Apis:ServiceBusApi").Value);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var jsonString = JsonConvert.SerializeObject(model);
                var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
                var reponseMessage = await client.PostAsync("api/subscription", httpContent);

                return new SuccessReponseModel
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "User is created.",
                    Payload = userModel
                };
            }
            throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthCannotCreate));
        }


        //[HttpPost("/api/[controller]/delete")]
        //public async Task<UserModel> Delete([FromBody] DeleteRequestModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        throw new HttpRequestException(string.Format(
        //            _errorHandler.GetMessage(ErrorMessagesEnum.ModelValidation),
        //            ModelState.Values.First().Errors.First().ErrorMessage));
        //    }
        //    var user = _usersService.GetByEmail(model.Email);
        //    if (user == null)
        //        throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthUserDoesNotExists));


        //    var result = await _usersService.Delete(user);
        //    if (result.Succeeded)
        //    {
        //        return user;
        //    }

        //    throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthCannotDelete));
        //}

        //[HttpPost]
        //public async Task<UserModel> Edit([FromBody] UpdateRequestModel request)
        //{
        //    var user = _usersService.GetByEmail(request.Email);

        //    if (user == null) throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthUserDoesNotExists));

        //    var validEmail = await _usersService.ValidateUser(user);

        //    if (!validEmail.Succeeded)
        //    {
        //        _errorHandler.ErrorIdentityResult(validEmail);
        //    }

        //    IdentityResult validPass = null;

        //    if (!string.IsNullOrEmpty(request.Password))
        //    {
        //        validPass = await _usersService.ValidatePassword(user, request.Password);
        //        if (validPass.Succeeded)
        //        {
        //            user.PasswordHash = _usersService.HashPassword(user,
        //                request.Password);
        //        }
        //        else
        //        {
        //            _errorHandler.ErrorIdentityResult(validPass);
        //        }
        //    }
        //    if (validPass != null && ((!validEmail.Succeeded || request.Password == string.Empty || !validPass.Succeeded)))
        //        throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthNotValidInformations));

        //    var result = await _usersService.Update(user);

        //    if (result.Succeeded)
        //    {
        //        return user;
        //    }

        //    throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthCannotUpdate));

    }

    //[HttpPost, Produces("application/json")]
    //public async Task<SignInResult> Token(TokenRequestModel request)
    //{

    //    if (!ModelState.IsValid)
    //    {
    //        throw new HttpRequestException(string.Format(_errorHandler.GetMessage(ErrorMessagesEnum.ModelValidation), ModelState.Values.First().Errors.First().ErrorMessage));
    //    }

    //    var user = _usersService.GetByEmail(request.Username);
    //    if (user == null) throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthUserDoesNotExists));


    //    await _usersService.SignOutAsync();
    //    var result = await _usersService.PasswordSignInAsync(
    //        user, request.Password, false, false);

    //    if (!result.Succeeded) throw new HttpRequestException(_errorHandler.GetMessage(ErrorMessagesEnum.AuthCannotRetrieveToken));

    //    // Create a new ClaimsIdentity holding the user identity.
    //    var identity = new ClaimsIdentity(
    //        OpenIdConnectServerDefaults.AuthenticationScheme,
    //        OpenIdConnectConstants.Claims.Name, null);
    //    // Add a "sub" claim containing the user identifier, and attach
    //    // the "access_token" destination to allow OpenIddict to store it
    //    // in the access token, so it can be retrieved from your controllers.
    //    identity.AddClaim(OpenIdConnectConstants.Claims.Subject,
    //        "71346D62-9BA5-4B6D-9ECA-755574D628D8",
    //        OpenIdConnectConstants.Destinations.AccessToken);
    //    identity.AddClaim(OpenIdConnectConstants.Claims.Name, user.UserName,
    //        OpenIdConnectConstants.Destinations.AccessToken);


    //    var principal = new ClaimsPrincipal(identity);
    //    return SignIn(principal, OpenIdConnectServerDefaults.AuthenticationScheme);
    //}

    
}